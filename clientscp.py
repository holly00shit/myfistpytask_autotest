#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
from socket import *
from time import ctime,sleep
from atpack import *
from authentication import Authentication
from configfile import ConfigFile
from convertsth import get_iut_from_user_input, get_cdr_from_file_user_select
from taskmsg import TaskMsg


def main():
   if len(sys.argv)<3 or sys.argv[1].lower()!="-i" or len(sys.argv[2])==0:
      print "Usage:%s -i configfile" %sys.argv[0]
      sys.exit()

   #init config file
   cfgfile = sys.argv[2]
   configfile = ConfigFile(cfgfile)
   configfile.initClientItem()

   address = (configfile.servHost, configfile.servPort)
   try:
       cs = socket(AF_INET, SOCK_STREAM)
       cs.connect(address)
   except error,e:
       print 'Fail to connect to server: %s,detail: %s' % (str(address),str(e))
       return 1
   
   #authentication pkg
   authen_msg = Authentication(cs,configfile.temp_ct,configfile.myName)
   authen_msg.send_authen_msg()
   rlt_code,msg = authen_msg.rev_authen_msg()
   if rlt_code != 0:
       print msg
       return 1

   #send task
   command = 272
   seq = 1
   task_msg = TaskMsg(cs,seq,command,configfile)
   cdr_select_xml = get_cdr_from_file_user_select(task_msg)
   while True:
       in_str = get_iut_from_user_input(configfile.bussness)
       if in_str == '0':
           sys.exit()
       elif in_str == '':
           continue
       send_stat = task_msg.send_task_msg(cdr_select_xml,in_str)
       if send_stat == -1:
           print "error happend when send msg to server!"
           sys.exit()
       rece_stat = task_msg.receive_server_msg()
       if rece_stat == 1:
           break

   return 0
      
if __name__ == '__main__':
    main()


