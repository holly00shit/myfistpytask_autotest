# -*- coding: utf-8 -*-

from socket import error

class SocketError(error):
    def __init__(self):
        pass