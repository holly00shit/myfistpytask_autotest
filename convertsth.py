#!/usr/bin/env python
# -*- coding: utf-8 -*-

DR_IN_GGPRS_RG = ['SERVICE_CODE','START_TIME','OCTETS1']

def display_cdr(cdrxml,busi_name):
    """Display cdr for user to choice
    """
    base = cdrxml.find('base')
    ext = cdrxml.find('ext')
    rgs = cdrxml.find('rgs')
    if base != None and len(base) > 0:
        print """[base]"""
        for bitem in base:
            print ("%s:".rjust(5).ljust(10) + "%s") %(bitem.tag,(bitem.text if bitem.text else ''))
    if ext != None and len(ext) > 0:
        print """[ext]"""
        for eitem in ext:
            print ("%s:".rjust(5).ljust(10) + "%s") %(eitem.tag,(eitem.text if eitem.text else ''))
    if busi_name == 'GPRS':
        print """[rgs]"""
        if rgs != None and len(rgs) > 0:
            for ritem in rgs:
                print ("%s:".rjust(5).ljust(10) + "%s") %(ritem.tag,(ritem.text if ritem.text else ''))

    return 1

def parase_user_input(in_str):
    """Param in:
          in_str: A / I,rg1,rg2 / U(123) /U,rg1(123),rg2(123) /T() /T,rg1(),rg2()...
    Param out:
          cc_req_type: A / I / U / T /
          rg_name_list: [rg1,rg2,...]
          cc_dict: {U:123,rg1:123,...]
    """
    cc_dict = {}
    cc_req_type = ''
    rg_name_list = []
    in_list = in_str.strip().strip(',').split(',')
    cc_type_str = in_list[0]


    if isinstance(cc_type_str,basestring) \
        and cc_type_str.startswith(('I','U','T','A')):
        cc_req_vol = matchParentheses(cc_type_str[1:],'(',')')
        cc_req_type = cc_type_str[0]
        if cc_req_vol != '':
            cc_dict[cc_req_type] = cc_req_vol

    if len(in_list) > 1:
        rgs = in_list[1:]
        for rg in rgs:
            if isinstance(rg,basestring):
                rg_vol = matchParentheses(rg,'(',')')
                rg_name = rg.split('(')[0]
                rg_name_list.append(rg_name)
                if rg_vol != '':
                    cc_dict[rg_name] = rg_vol

    return cc_req_type,rg_name_list,cc_dict

def matchParentheses(dataStr, leftF, rightF):
    """Match leftF and rightF,
    Return Str of leftF to rightF in dataStr ,
    The num of rightF in dataStr.
    """
    if len(dataStr) <= 0:
        return ''
    ibeg = 0
    iend = 0
    n = 0
    for item in range(len(dataStr)):
        if dataStr[item] == leftF:
            n += 1
            if n ==1:
                ibeg = item
        elif dataStr[item] == rightF:
            n -= 1
            if n == 0:
                iend = item
                break
        else:
            return ''
    return dataStr[ibeg+1:iend]

def connect_value_to_name(xdr_tags):
    """Return:
        dict{'xdr_name':xdr_value
            ...
            }
    """
    rls_dict = {}
    for xtags in xdr_tags:
        for xtag in xtags:
            try:
                rls_dict[xtag.tag] = xtag.text
            except:
                print 'err happen in [connect_value_to_name]'
                return {}
    return rls_dict

def get_dict_to_fv(tdict,rg_list,mode=1):
    """mode=1:only to get tfv when value in tdict is null
       mode=0:get all tfv of tdict
    """
    rls_str = ''
    rg_str = ''
    # for (key,value) in zip(tdict.iterkeys(),tdict.iterkeys()):
    for fname in tdict.iterkeys():
        if mode == 1 and tdict[fname] != None and tdict[fname] != '':
            rls_str += fname + ':' + tdict[fname] + ';'

    for rg in rg_list:
        tt = ''
        for (rg_name,rg_value) in zip(DR_IN_GGPRS_RG,rg):
            tt += rg_name + ':' + rg_value + ';'
        rg_str += '(' + tt + ')'
    return rls_str,rg_str

def get_iut_from_user_input(buss_name):
    """Get I/U/T and num of RG to package msg
    """
    if buss_name == 'GPRS':
        print "please select A / I / U / T and RG,like: I, rg1, rg2..."
    elif buss_name == 'GSM':
        print "please select A / I / U / T "
    elif buss_name == 'SMS':
        print "please select I"
    in_str = raw_input('scp>>')
    if not in_str.startswith(('A','I','U','T')) and in_str != '0':
        print "unexpect input !!!"
        return ''
    return in_str

def get_cdr_from_file_user_select(taskmsgobj):
    """get the selected cdr from file
    """
    infoLevel_1 = """
    Enter 1~%s to select cdr#,0 to exit
    """
    if taskmsgobj.business == 'GPRS':
        # cdrList = taskmsgobj.readcdrfile(taskmsgobj.configfile.cdrFile)
        cdrList = taskmsgobj.read_new_cdr_file(taskmsgobj.configfile.cdrFile)
    else:
        cdrList = taskmsgobj.read_new_cdr_file(taskmsgobj.configfile.cdrFile)
    print infoLevel_1 % len(cdrList)
    iNum = 1
    while True:
        selt = raw_input('scp>>')
        iNum = taskmsgobj.check_input(selt,len(cdrList))
        if iNum == -1:
            continue
        elif iNum == 0:
            return -1
        else:
            break
    cdr_select = cdrList[iNum-1]
    display_cdr(cdr_select,taskmsgobj.business)
    return cdr_select