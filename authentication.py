#!/usr/bin/env python
# -*- coding: utf-8 -*-
from atpack import getRegistPkgReq, recvAtpackage, NOTIFY, AtMsgCtlPack
from socket import error

class Authentication():
    """To send authentication msg to server
    """

    def __init__(self,cs,temp_ct,my_name):
        """Init msg
        """
        self.connType = 0 # default short connect
        if temp_ct != "":
            self.connType = int(temp_ct)
        self.my_name = my_name
        self.cs = cs


    def send_authen_msg(self):
        """Send msg
        """
        pkgData = getRegistPkgReq(self.my_name,self.connType)
        try:
            self.cs.sendall(pkgData)
        except error, e:
            print str(e)
            self.cs.close()
            return

    def rev_authen_msg(self):
        """receive authen msg from server!
        """
        rlt_code = 0
        msg = ''
        relcode,recvData,ctlLen,msgType = recvAtpackage(self.cs)
        if relcode in (1,2):
            msg = 'fail to auth'
            rlt_code = 1
            self.cs.close()
        else:
            if msgType != NOTIFY['REGISTER_A']:
                msg = 'recv error answer'
                rlt_code = 1
                self.cs.close()

            retPack = AtMsgCtlPack()
            retPack.setPackData(recvData)
            result = retPack.getKey('result')
            if result != 'success':
                msg = retPack.getKey('msg')
                rlt_code = 1
                self.cs.close()

        return rlt_code,msg