#!/usr/bin/env python
#coding=gbk

import sys,os
import logging
import time
from basecfg import *
from connector import *
from atpack import DCCTYPE
from dccFunc.interFaceofDccEnDe import PrtCdrDict
from dcclib import *

class gprscfg(basecfg):
    def __init__(self, hlog, destIP, destPORT):
        basecfg.__init__(self)
        self.conn = connector(hlog, destIP, destPORT)
        if not self.conn.connectTo():
            self.conn = None

            #logflag = 0 dcc stream; 1 dump dcc detail; 2 cdr str
        self.logflag = 2
        self.timerate = 60

        base_head_fmt = """
[263;enc_OctetString;;0;0;FATHER_SESSION_ID],
[264;enc_OctetString;;0;0;ORIGIN_HOST],
[296;enc_OctetString;;0;0;ORIGIN_REALM],
[278;enc_Unsigned32;;0;0;ORIGIN_STATE_ID],
[283;enc_OctetString;;0;0;DESTINATION_REALM],
[258;enc_Unsigned32;;0;0;AUTH_APPLICATION_ID],
[461;enc_OctetString;;0;0;SERVICE_ID],
[416;enc_Integer32;;0;0;FATHER_PROCESS_TYPE],
[415;enc_Unsigned32;;0;0;CC_REQUEST_NUMBER],
[293;enc_OctetString;;0;0;DESTINATION_HOST],
[55;enc_UTCTime_2;;0;0;START_TIME],
"""
        base_tail_fmt = """
[458;;;0;0;]([459;enc_Unsigned32;;0;0;USER_EQUIPMENT_INFO_TYPE],
             [460;enc_OctetString;;0;0;USER_EQUIPMENT_INFO_VALUE])
[873;;10415;1;0;]([874;;10415;1;0;]([2;enc_Unsigned32;10415;1;0;CHARGING_ID],
                                    [3;enc_Integer32;10415;1;0;PDP_TYPE],
                                    [1227;enc_Address;10415;1;0;PDP_ADDRESS],
                                    [5;enc_OctetString;10415;1;0;QOS_PROFILE],
                                    [1228;enc_Address;10415;1;0;SGSN_ADDRESS],
                                    [847;enc_Address;10415;1;0;GGSN_ADDRESS],
                                    [8;enc_OctetString;;0;0;APN_NOI],
                                    [10;enc_OctetString;;0;0;NSAPI],
                                    [12;enc_OctetString;;0;0;SELECT_MODE],
                                    [18;enc_OctetString;;0;0;SGSN_PLAN],
                                    [23;enc_OctetString;;0;0;TIMEZONE],
                                    [22;enc_OctetString;;0;0;USER_LOCATION_INFO],
                                    [21;enc_OctetString;;0;0;RAT_TYPE],
                                    [30;enc_OctetString;;0;0;APN_NI],
                                    [13;enc_Unsigned32;10415;1;0;CHARGING_CHARACTERISTICS])
                 )
"""
        user_info = """
[443;;;0;0;##_1]([450;enc_Unsigned32;;0;0;USER_NUMBER_TYPE],
                 [444;enc_OctetString;;0;0;USER_NUMBER])
"""
        rategroup_info_i = """
[456;;;0;0;##_2]([432;enc_Unsigned32;;0;0;COMBINE_CODE],
                 [437;;;0;0;]([420;enc_Unsigned64;;0;0;COMBINE_CODE;D1024])
                )
"""
        rategroup_info_u = """
[456;;;0;0;##_2]([872;enc_Integer32;;0;0;REPORTING_REASON],
                 [432;enc_Unsigned32;;0;0;COMBINE_CODE],
                 [437;;;0;0;]([420;enc_Unsigned64;;0;0;COMBINE_CODE;D1024])
                 [446;;;0;0;]([421;enc_Unsigned64;;0;0;COMBINE_CODE],
                              [412;enc_Integer32;;0;0;COMBINE_CODE])
                )
"""
        rategroup_info_t = """
[456;;;0;0;##_2]([432;enc_Unsigned32;;0;0;COMBINE_CODE],
                 [446;;;0;0;##_3]([421;enc_Unsigned64;;0;0;COMBINE_CODE],
                                  [412;enc_Integer32;;0;0;COMBINE_CODE])
                )
"""
        base_head_ext_i = """
[455;enc_Unsigned32;;0;0;MULTIPLE_SERVICES_INDICATOR],
"""
        base_head_ext_t = """
[295;enc_Unsigned32;;0;0;TERMINATION_CAUSE],
"""
        respfmt = """
"""
        self.pkgFormt[272,'I'] = self.upperXdrname(''.join(
            (base_head_fmt+base_head_ext_i+user_info+base_tail_fmt).split() ))
        self.pkgFormt[272,'IRG'] = self.upperXdrname(''.join(
            (base_head_fmt+base_head_ext_i+user_info+rategroup_info_i+base_tail_fmt).split() ))
        self.pkgFormt[272,'U'] = self.upperXdrname(''.join(
            (base_head_fmt+user_info+rategroup_info_u+base_tail_fmt).split() ))
        self.pkgFormt[272,'UT'] = self.upperXdrname(''.join(
            (base_head_fmt+user_info+rategroup_info_t+base_tail_fmt).split() ))
        self.pkgFormt[272,'T'] = self.upperXdrname(''.join(
            (base_head_fmt+base_head_ext_t+user_info+rategroup_info_t+base_tail_fmt).split() ))
        self.pkgFormt[272,'A'] = self.upperXdrname(''.join(respfmt.split()))

    def analyse(self,dictData):
        """For Init Message.
        Add some infomation which is Necessary.
        """
        if 'START_TIME' in dictData.keys() and \
                        len(dictData['START_TIME']) > 0:
            pass
        else:
            dictData['START_TIME'] = time.strftime("%Y%m%d%H%M%S",time.localtime())

        dictData['CC_REQUEST_NUMBER'] = 1
        dictData['PROCESS_TYPE'] = 1
        dictData['ADDUP_RES'] = 0
        dictData['ERR_CODE'] = 2999

        if 'SESSION_ID' in dictData.keys():
            pass
        else:
            dictData['SESSION_ID'] = self.genSessionID()


    ###--------------------Api-------------------

    def needUpdate(self,dictData):
        """Judge by gprs logic if is end!!!
        """
        isEnd = 0
        isNeed = True
        if 'ERR_CODE' in dictData.keys() and int(dictData['ERR_CODE']) > 2999:
            isEnd = 2

        return (isNeed,isEnd)

    def updateData(self,command,byteData,dictData):
        """Update data received from inbossd.
        """
        dictData['CC_REQUEST_NUMBER'] += 1
        if dictData['PROCESS_TYPE'] == 1:  #init,416
            dictData['STIME'] = time.time()

        if 'DURATION' not in dictData.keys() or \
                        int(dictData['ADDUP_RES']) >= int(dictData['DURATION']):
            dictData['PROCESS_TYPE'] = 3  #termination
        else:
            dictData['PROCESS_TYPE'] = 2  #update

#        answerStr = prtCdr(self.pkgFormt[(command,'A')],len(byteData),byteData)        #����ת��xdr
#        answerDict = str2Dict('fvpair',answerStr)
        answerDict = PrtCdrDict(self.pkgFormt[(command,'A')],len(byteData),byteData)
        dictData['ERR_CODE'] = int(answerDict['ERR_CODE'])
        if int(answerDict['ERR_CODE']) >= 2000 and int(answerDict['ERR_CODE']) < 3000 and \
                        'IN_BUDGET_RES' in answerDict.keys():
            dictData['ADDUP_RES'] = int(dictData['ADDUP_RES']) + int(answerDict['IN_BUDGET_RES'])

    def getDccStream(self,command,dictData):
        """Get xdr dict to dcc stream
        """

    def prtDccStr(self,command,byteData):
        """
        """
