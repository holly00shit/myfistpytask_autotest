#!/usr/bin/env python
#coding=gbk

from time import sleep
import os,sys
from socket import *
from atpack import *

def setTaskPkgReq(taskPack,tupleHead):
   #authentication pkg
   if len(tupleHead) < 7:
      return False

   taskPack.setKey('seq',tupleHead[0])
   taskPack.setKey('desthost',tupleHead[1])
   taskPack.setKey('destport',tupleHead[2])
   taskPack.setKey('bussness',tupleHead[3])
   taskPack.setKey('datatype',tupleHead[4])
   taskPack.setKey('command',tupleHead[5])
   taskPack.setKey('opertype',tupleHead[6])

   return True

   
def main():
   servHost = '127.0.0.1'
   servPort = 65199
   address = (servHost, servPort)
   
   try:
       cs = socket(AF_INET, SOCK_STREAM)
       cs.connect(address)
   except error,e:
       print 'Fail to connect to server: %s,detail: %s' % (str(address),str(e))
       return 1
   
   #authentication pkg
   pkgData = getRegistPkgReq('test',0)
   msg = ''
   try:
      cs.sendall(pkgData)
   except error, e:
      msg = str(e)
      print msg
      cs.close()
      return 1
   
   relcode,recvData,ctlLen,msgType = recvAtpackage(cs)
   if relcode in (1,2):
      print 'fail to auth'
      cs.close()
   else:
      if msgType != NOTIFY['REGISTER_A']:
         print 'recv error answer'
         cs.close()
      
      retPack = AtMsgCtlPack()
      retPack.setPackData(recvData)
      result = retPack.getKey('result')
      if result != 'success':
         print retPack.getKey('msg')
         cs.close()
      
      #send task
      inbossdHost = '10.10.10.172'
      inbossdPort = 32500
      bussness = 'GSM'
      command = 272
      opertype = DCCTYPE['ALL']
      vals = "SESSION_ID:cmcc.scp.172.1;USER_NUMBER:13519302744;OPP_NUMBER:0930114;LAC_ID:24da;CELL_ID:1a;MSC_ID:8613900571;CALL_TYPE:0;"
   
      seq = 1
      datatype = 'fvpair'
      headInfo = (seq,inbossdHost,inbossdPort,bussness,datatype,command,opertype)
      taskPack = AtMsgCtlPack()
      setTaskPkgReq(taskPack,headInfo)
      taskPack.setKey('data',vals)
      
      xmlData = taskPack.getPackData()
      headData = packageHead(len(xmlData),NOTIFY['TASK_R'])
      
      msg = ''
      try:
         cs.sendall(headData+xmlData)
      except error, e:
         msg = str(e)
      
      isEnd = '1'
      while isEnd == '1':
          relcode,recvData,ctlLen,msgType = recvAtpackage(cs)
          if relcode in (1,2):
             print 'fail to task'
             print recvData
             cs.close()
          else:
             if msgType != NOTIFY['TASK_A']:
                print 'recv error answer'
                cs.close()
             
             retPack = AtMsgCtlPack()
             retPack.setPackData(recvData)
             rltseq = retPack.getKey('seq')
             isEnd = retPack.getKey('isend')
             if rltseq != seq:
                 print 'answer not request'
             else:
                 result = retPack.getKey('result')
                 if result != 'success':
                    print retPack.getKey('msg')
                    cs.close()
                 else:
                    print retPack.getKey('data')
                
   return 0
      
if __name__ == '__main__':
    main()


