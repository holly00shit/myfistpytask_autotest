#!/usr/bin/env python
#coding=gbk

from socket import *
import sys,os
import logging
import threading
from time import sleep, ctime
from string import *
from atpack import *
from inbossdpool import *
from dcclib import *
    
class getTask(threading.Thread):
    def __init__(self, cs,hsyslog,subSiglList,subContList,slock = None,clock=None):
        threading.Thread.__init__(self)
        self.conn = cs
        self.syslog = hsyslog
        self.siglList = subSiglList
        self.contList = subContList

        #self.slock = slock
        #self.clock = clock
        
    def __call__(self):
        while True:
            errorMsg = ''
            relcode,recvData,ctlLen,msgType = recvAtpackage(self.conn)
            if relcode in (1,2):
                self.syslog.error('Error in dealing task: recv data: %s' % recvData)
                break
            else:
                if msgType != NOTIFY['TASK_R']:
                    self.syslog.info('here hope a task pkg,but recv: %s' % msgType)
                    rlt_code,msg = answer(self.conn,'fail',NOTIFY['TASK_A'])
                else:
                    self.syslog.debug('recive data: %s'%recvData)
                    retpack = AtMsgCtlPack()
                    retpack.setPackData(recvData)
                    
                    sequence = retpack.getKey('seq')
                    destHost = retpack.getKey('desthost')
                    destPort = retpack.getKey('destport')
                    bussness = retpack.getKey('bussness')
                    dataType = retpack.getKey('datatype')
                    command = retpack.getKey('command')
                    operType = retpack.getKey('opertype')
                    if dataType == 'fvpair' or dataType == 'stream':
                        datastr = retpack.getKey('data')
                    else:
                        datastr = retpack.getList('data')
                    dictData = str2Dict(dataType,datastr)
                    record = (sequence,self.conn,destHost,destPort,bussness,dataType,
                              command,operType,datastr,dictData,0)
                    self.syslog.debug('recive data ALL[0]/SIGL[1] : %s' % operType)
                    self.syslog.debug('recive data : %s' % datastr)
                    if operType == DCCTYPE['ALL']:
                        #with self.slock:
                        self.contList.append(record)
                    else:
                        #with self.clock:
                        self.siglList.append(record)

class dealSiglTask(threading.Thread):
    def __init__(self, hsyslog, subSiglList, slock):
        threading.Thread.__init__(self)
        self.syslog = hsyslog
        self.siglList = subSiglList
        self.slock = slock
        self.cfgPool = inbossdpool(5,self.syslog)

    def __call__(self):
        while True:
            ll = len(self.siglList)
            if ll > 0:
                sequence = self.siglList[0][0]
                scpConn = self.siglList[0][1]
                destHost = self.siglList[0][2]
                destPort = self.siglList[0][3]
                bussness = self.siglList[0][4]
                dataType = self.siglList[0][5]
                command = self.siglList[0][6]
                dictData = self.siglList[0][9]

                cfgObj = self.cfgPool.getConn(destHost,destPort,bussness)
                if cfgObj == None:
                    self.syslog.error('Not regist:(%s,%s,%s)',destHost,destPort,bussness)
                else:
                    dccStream = ''
                    if dataType != 'stream':
                        dccStream = cfgObj.getDccStream(int(command),dictData)
                    else:
                        dccStream = self.siglList[0][8]
                    self.syslog.debug('Send to inbossd: %s' %dccStream)
                    cfgObj.conn.send(dccStream)
                    rltcode,byteData,dataLen,cmdcode = cfgObj.conn.recv()
                    result = 'fail'
                    datastr = str(byteData)
                    if rltcode == 0:
                        answer = cfgObj.prtDccStr(int(cmdcode),byteData)
                        result = 'success'
                        datastr = answer
                    self.syslog.debug('Recieve from inbossd:%s' %datastr)
                    answerTask(scpConn,sequence,result,datastr)
                    
                    self.cfgPool.putConn(destHost,destPort,bussness)
                
                with self.slock:
                    del self.siglList[0]

            else :
                sleep(1)
        
        self.syslog.error('finished at:'+ ctime())
            
class dealContTask(threading.Thread):
    def __init__(self, hsyslog,subContList, slock):
        threading.Thread.__init__(self)
        self.syslog = hsyslog
        self.contList = subContList
        self.slock = slock
        self.cfgPool = inbossdpool(5,self.syslog)

    def __call__(self):
        while True:
            ll = len(self.contList)
            ipos = 0
            while ipos < ll:
                sequence = self.contList[ipos][0]
                scpConn = self.contList[ipos][1]
                destHost = self.contList[ipos][2]
                destPort = self.contList[ipos][3]
                bussness = self.contList[ipos][4]
                dataType = self.contList[ipos][5]
                command = self.contList[ipos][6]
                dictData = self.contList[ipos][9]

                cfgObj = self.cfgPool.getConn(destHost,destPort,bussness)
                rltcode,isEnd = cfgObj.needUpdate(dictData)

                self.syslog.debug('need update : %s ,Endcode : %d'% (rltcode,isEnd))
                if rltcode and isEnd == 1:
                    dccStream = cfgObj.getDccStream(int(command),dictData)
                    self.syslog.debug('SEND to (%s,%s,%s): %s' % (destHost,destPort,bussness,dccStream))
                    cfgObj.conn.send(dccStream)
                    rltcode,byteData,dataLen,cmdcode = cfgObj.conn.recv()
                    result = 'fail'
                    datastr = str(byteData)
                    if rltcode == 0:
                        answer = cfgObj.prtDccStr(int(cmdcode),byteData)
                        result = 'success'
                        datastr = answer
                    flag = '1'
                    if isEnd:
                        flag = '0'

                    self.syslog.debug('Answer to Task:%s, %s ,END!!' %(result, datastr))
                    answerTask(scpConn,sequence,result,datastr,flag)
                    if rltcode == 0:
                        self.syslog.debug('Need to be decoded Dcc Msg: %s ,END!!' % byteData)
                        cfgObj.updateData(int(command),byteData,dictData)
                        #self.contList[ipos][9] = dictData
                
                if isEnd in (1,2):
                    with self.slock:
                        del self.contList[ipos]
                    ll -= 1
                else:
                    ipos += 1
                        
                self.cfgPool.putConn(destHost,destPort,bussness)
            
            sleep(1)
        
        self.syslog.error('finished at:'+ ctime())

