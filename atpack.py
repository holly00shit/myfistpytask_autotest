#!/usr/bin/env python
#coding=gbk

from xml.etree.ElementTree import ElementTree,ParseError
from xml.etree.ElementTree import Element, fromstring, tostring, SubElement
from socket import timeout,error

NOTIFY={'REGISTER_R':'100','REGISTER_A':'101','TASK_R':'110','TASK_A':'111'}
DCCTYPE={'ALL':'0','SIGL':'1'}
CCREQTYPE = {'A':'1','I':'1','U':'2','T':'3'}
RELCODE=('finished','failed')
#SOCKETSTS=('TIMEOUT','CLOSED')
ATPHEAD_FSIZE = 8
ATPHEAD_SIZE = 11
TIME_OUT = 80

def packageHead(dataLen,command):
    return (ATPHEAD_FSIZE - len(str(dataLen))) * '0'+ str(dataLen) \
           + str(command)

def recvAtpackage(cs):
    if cs == None:
        return (1,'socket is null',0,0)

    try:
        recvData = cs.recv(ATPHEAD_SIZE)
    except timeout,e:
        return (1,'TIMEOUT',0,0)
    except error,e :
        return (1,'socket error during recv head: %s ' % e,0,0)

    if not recvData:
        return (1,'socket is the other closed',0,0)

    pkgLen = 0
    msgType = '0000'
    if (recvData[0:ATPHEAD_FSIZE]).isdigit():
        pkgLen=int(recvData[0:ATPHEAD_FSIZE])
        msgType = recvData[-3:]

    else:
        return (1,'recv package(head) error: %s' % recvData,pkgLen,msgType)

    #循环收包一直把pkLen长度的包收完
    if pkgLen > 0:
        recvData = ''
        try :
            rnbytes = 0
            while rnbytes < pkgLen:
                nsize = 0
                recvbuffer = bytearray(pkgLen-rnbytes)
                nsize = cs.recv_into(recvbuffer)
                rnbytes += nsize
                recvData += str(recvbuffer[:nsize])
        except error,e:
            return (1,'socket error during recv body: %s ' % e,pkgLen,msgType)
    else:
        #invalid head
        return (2,recvData,pkgLen,msgType)

    return (0,recvData,pkgLen,msgType)

def answer(cs,result,command):
    sendPack = AtMsgCtlPack()
    sendPack.setKey('result',result)
    xmlData = sendPack.getPackData()
    head = packageHead(len(xmlData),command)
    
    msg = ''
    try:
       cs.sendall(head + xmlData)
    except error, e:
       msg = str(e)
    
    if len(msg) > 0:
        return False,msg
    else:
        return True,'it is ok'
           
def answerTask(cs,sequence,result,datastr,isend = '0'):
    sendPack = AtMsgCtlPack()
    sendPack.setKey('seq',sequence)
    sendPack.setKey('result',result)
    sendPack.setKey('isend',isend)
    if result == 'success':
        sendPack.setKey('data',str(datastr))
    else:
        sendPack.setKey('msg',str(datastr))
        
    xmlData = sendPack.getPackData()
    head = packageHead(len(xmlData),NOTIFY['TASK_A'])
    
    msg = ''
    try:
       print 'inboss send to client :' ,head+xmlData
       cs.sendall(head + xmlData)
    except error, e:
       msg = str(e)
    
    if len(msg) > 0:
        return False,msg
    else:
        return True,'it is ok'

class AtMsgCtlPack(object):       #构造xml格式报文
      def __init__(self):            
          self.rootElement = Element('Root')            
          self.eTree = ElementTree()
          self.eTree._setroot(self.rootElement)                                         

      def setPackData(self,data):
          #print 'setPackData: %s' % data
          try:
              self.rootElement = fromstring(data)
          except ParseError,e:
              return 1,'%s' % str(e)

          self.eTree._setroot(self.rootElement)
          return 0, 'it is ok'

      def getPackData(self):
          #self.clearEmptyElement()
          #self.eTree.write("test_package.xml", encoding="utf-8", xml_declaration=True, method="xml")
          return '<?xml version=\'1.0\' encoding=\'utf-8\'?>\n' + tostring(self.rootElement)

      def clearEmptyElement(self):
          def childCount(element):
              i = 0
              for node in element:
                    i = i + 1
              return i

          def clearEle(element):
              if element is None:
                   return 0

              print 'Enter tag:'  + element.tag

              for node in element:
                  if node is not None:
                       ret = clearEle(node)                       
                       if ret == 1:
                           return 1

                  if node.text == None  and node.attrib == {} and childCount(node) == 0:
                      print 'remove a element:'+ node.tag
                      element.remove(node)
                      return 1
              return 0

          while True:
              if clearEle(self.rootElement) == 0:
                  break

      def setKey(self,keyName,keyValue):
          aElement = self.rootElement.find(keyName) 
          if aElement is not None:
              aElement.text = str(keyValue)
          else: 
              aElement = SubElement(self.rootElement,keyName)
              aElement.text = str(keyValue)

      def getKey(self,keyName):
          aElement = self.eTree.find(keyName)
          if aElement is not None:
              return aElement.text
          else:
              return ''

      def getList(self,keyName):
          aElement = self.eTree.find(keyName)
          if aElement is not None:
             rltstr = ''
             for child in aElement:
                 rltstr += '%s:%s;' % (child.tag,child.text)
             return rltstr
          else:
              return ''
          
def getRegistPkgReq(clientName,connType):
   #authentication pkg
   flag = 1
   if connType != flag:
       flag = 0
       
   authPack = AtMsgCtlPack()
   authPack.setKey('name',clientName)
   authPack.setKey('type',flag)
   xmlData = authPack.getPackData()
   
   return packageHead(len(xmlData),NOTIFY['REGISTER_R']) + xmlData
    
def main():
    pass

if __name__ == '__main__':
      main()
