#!/usr/bin/env python
#coding=gbk

#from dcc_codeD import *

# beginPos is the position of '(' + 1
from dccFunc.baseFunc import matchParentheses


def parseValues(dataStr):
    dictData = {}
    if len(dataStr) <= 0:
        return dictData

    iBegin = 0
    strKey = ''
    strValue = ''
    iEnd = len(dataStr)
    while iBegin < iEnd:
        if dataStr[iBegin:iBegin+3] == '##_':
            nextPos = dataStr.find('{',iBegin)
            strKey = dataStr[iBegin:nextPos]
            iLen,strValue = matchParentheses(dataStr[nextPos+1:],'{','}')
            iBegin = nextPos + iLen + 3 #{};
        else:
            nextPos = dataStr.find(';',iBegin)
            if nextPos < 0:
                nextPos = iEnd
            strKey = dataStr[iBegin:dataStr.find(':',iBegin)]
            strValue = dataStr[dataStr.find(':',iBegin)+1:nextPos]
            iBegin = nextPos + 1

        dictData[strKey.upper().strip()] = strValue

    return dictData

def str2Dict(dataType,dataStr):
    dictData = {}
    if dataType == 'xml':
       #<field>value</field>...
       return dictData
    elif dataType == 'fvpair':
        #field:value;...
        return parseValues(dataStr)


