#!/usr/bin/env python
#coding=gbk

from socket import *
import sys,os
import logging
import threading

from seduscpcfg import registInbossd
from gsmcfg import *


class inbossdpool(object):
    def __init__(self, maxSize, hsyslog):
        self.maxSize = maxSize
        #(host,ip,bussness)->obj,lock(user num)
        self.objDict = {}
        self.hlog = hsyslog
    
    def getConn(self,destHost,destPort,bussness):
        if (destHost,destPort,bussness) in self.objDict.keys():
            self.objDict[(destHost,destPort,bussness)][1] += 1
            return self.objDict[(destHost,destPort,bussness)][0]
        
        if (destHost,destPort,bussness) in registInbossd.keys():
            destCls = registInbossd[(destHost,destPort,bussness)]
            inbossd = [eval(destCls+"""(self.hlog, destHost,destPort)"""),0]
            if inbossd[0].conn == None:
                return None
            
            self.objDict[(destHost,destPort,bussness)] = inbossd
            return self.objDict[(destHost,destPort,bussness)][0]
        else:
            return None
        
    def putConn(self,destHost,destPort,bussness):
        if (destHost,destPort,bussness) in self.objDict.keys():
            self.objDict[(destHost,destPort,bussness)][1] -= 1
        else:
            self.hlog.error("put conn error:%s,%s,%s" % (destHost,destPort,bussness))

