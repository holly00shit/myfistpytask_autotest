# -*- coding: utf-8 -*-
#!/usr/bin/env python

#############################################
# Author : zhongks@aisainfo-linkage.com
# Date   : 2013/01/24
# Brief  : implenment class CDCCTree
#############################################
from dccFunc.baseFunc import *
from dccFunc.dccAvp import PDCCAVP
from dccFunc.dccDefine import *
from dccFunc.dccHead import PDCCHEAD

class PDCCTREE:
    """Include head and avp body. """
    def __init__(self):
        self.m_tDccAvp = () # 保存所有解出来的avp对象
        self.m_oDccHead = PDCCHEAD()
        self.m_oDccAvp  = PDCCAVP()
        self.m_dAvpName = {} # 保存avp对象的名称，可以根据avp名称判断父子节点，也可以通过name访问avp对象

    def paraseDccHead(self,m_sDccHeadMsg):
        return self.m_oDccHead.decode(m_sDccHeadMsg)

    def decodeAvpintoTuple(self,m_imsgLen,m_sDccAvpMsg):
        """解包拆包 avp报文组解析成单个avp对象的list."""
        m_lDccAvpMsg = [m_sDccAvpMsg]
        iRet = 0

        # dcc报文头解码
        m_sDccHeadMsg = m_lDccAvpMsg[0][0:DCC_HEAD_LEN]
        iRet = self.paraseDccHead(m_sDccHeadMsg)
        if iRet == 0:
            m_lDccAvpMsg[0] = m_lDccAvpMsg[0][DCC_HEAD_LEN:]
            if m_imsgLen != self.m_oDccHead.m_iLength*2:
                print 'some err in dcc msg : msglen :%d, dcclen:%d' \
                      %(m_imsgLen,self.m_oDccHead.m_iLength*2)
        else:
            print 'err happend when decode dcc head',iRet
            return iRet

        # avp结构解码
        m_iavpSqe = 0
        while m_imsgLen >= AVP_HEAD_LEN:
            iRet = self.m_oDccAvp.decode(m_lDccAvpMsg)
            if iRet == 0 or iRet == 99:
                m_newavp = 'm_dccAvp' + '_' + str(m_iavpSqe)
                self.m_dAvpName[m_newavp] = PDCCAVP()
                self.copyPDccAvp(self.m_oDccAvp,self.m_dAvpName[m_newavp])
                self.m_tDccAvp += (self.m_dAvpName[m_newavp],)
                m_iavpSqe += 1
                m_imsgLen -= self.m_oDccAvp.m_iLength*2

                if iRet == 99:
                    m_iLength = self.m_oDccAvp.m_iLength - self.m_oDccAvp.getAvpHeadLen() # 复合avp长度
                    self.m_oDccAvp.clear() # new avp begin
                    m_iavpSubSqe = 0
                    m_newavpSub = ''
                    while m_iLength >= AVP_HEAD_LEN:
                        m_newavpSub = m_newavp +  '_' + str(m_iavpSubSqe)
                        self.m_dAvpName[m_newavpSub] = PDCCAVP()
                        iRet = self.m_oDccAvp.decodeGrouped(m_lDccAvpMsg,m_iLength)
                        if iRet == 0:
                            m_iLength -= self.m_oDccAvp.m_iLength
                        elif iRet == 99:
                            m_iLength -= self.m_oDccAvp.getAvpHeadLen()
                        else:
                            print 'err happened when decode,err_code: %d' %iRet,self.m_dAvpName[m_newavpSub].m_iCode
                            return DIAMETER_AVP_UNSUPPROTED
                        self.copyPDccAvp(self.m_oDccAvp,self.m_dAvpName[m_newavpSub])
                        self.m_tDccAvp += (self.m_dAvpName[m_newavpSub],)
                        m_iavpSubSqe += 1
                        self.m_oDccAvp.clear()
        return iRet

    def copyPDccAvp(self,oriDccAvp,newDccAvp):
        newDccAvp.m_iCode     = oriDccAvp.m_iCode
        newDccAvp.m_iVendorId = oriDccAvp.m_iVendorId
        newDccAvp.m_bFlagsV   = oriDccAvp.m_bFlagsV
        newDccAvp.m_bFlagsM   = oriDccAvp.m_bFlagsM
        newDccAvp.m_bFlagsP   = oriDccAvp.m_bFlagsP
        newDccAvp.m_iLength   = oriDccAvp.m_iLength
        newDccAvp.m_iAddLength = oriDccAvp.m_iAddLength
        newDccAvp.m_sDataType = oriDccAvp.m_sDataType
        newDccAvp.m_sName     = oriDccAvp.m_sName
        newDccAvp.m_sValue    = oriDccAvp.m_sValue
        newDccAvp.m_sNext     = oriDccAvp.m_sNext
        newDccAvp.m_bParentFlag  = oriDccAvp.m_bParentFlag
        newDccAvp.m_sSub      = oriDccAvp.m_sSub
        newDccAvp.m_bOriMsg   = oriDccAvp.m_bOriMsg
        return 0

    def encodeAvpintoTuple(self,avpEnList,avpSqoList,dictData):
        """Put the data in dictData
        TO PDCCTREE.m_tDccAvp
        """
        avpLong = 0
        objList = []
        for i in range(len(avpEnList)-1,-1,-1):
            avpEnList[i] = avpEnList[i].split(';')
            avpCode = int(avpEnList[i][0])
            avpVendorId = AvpCode_VendorId_Map[avpCode]
            if avpVendorId:
                avpFlagsV = True
            else:
                avpFlagsV = False
            avpFlagsM = True
            avpFlagsP = False
            avpLength = 0
            avpName = avpEnList[i][5]
            try:
                avpvalue = dictData[avpName] # if not have key avpName!!
            except KeyError,e:
                avpvalue = ''
            self.m_oDccAvp.setAvp([avpCode,avpVendorId,avpFlagsV,
                                   avpFlagsM,avpFlagsP,avpLength,avpName,avpvalue])
            EncodeAvpValue(self.m_oDccAvp) # encode avp value
            # calculate avp length
            if i == len(avpEnList)-1: # the last avp in list
                avpLength = self.m_oDccAvp.getAvpHeadLen() +\
                            len(self.m_oDccAvp.m_sValue)/2
                self.m_oDccAvp.m_bParentFlag = 1
                if avpSqoList[i] != 0:
                    avpLong += self.m_oDccAvp.m_iLength
            elif avpSqoList[i] != 0:
                if avpSqoList[i] < avpSqoList[i+1]:
                    avpLength = avpLong + self.m_oDccAvp.getAvpHeadLen()
                elif avpSqoList[i] == avpSqoList[i+1]:
                    avpLength = self.m_oDccAvp.getAvpHeadLen() +\
                                len(self.m_oDccAvp.m_sValue)/2
                    self.m_oDccAvp.m_bParentFlag = 2 # Not the highset level
                elif avpSqoList[i] > avpSqoList[i+1]:
                    avpLong = 0
                avpLong += self.m_oDccAvp.m_iLength
            elif avpSqoList[i] == 0 and avpSqoList[i+1] != 0:
                avpLength = avpLong + self.m_oDccAvp.getAvpHeadLen()
            elif avpSqoList[i] == 0 and avpSqoList[i+1] == 0:
                avpLength = self.m_oDccAvp.getAvpHeadLen() +\
                            len(self.m_oDccAvp.m_sValue)/2
                self.m_oDccAvp.m_bParentFlag = 1 # The highest level
            else:
                print 'out of order !!' ,i
            self.m_oDccAvp.m_iLength = avpLength # Reset Length
            savpName = 'avp'+'_'+str(i)
            self.m_dAvpName[savpName] = PDCCAVP()
            self.copyPDccAvp(self.m_oDccAvp,self.m_dAvpName[savpName])
            objList.append(self.m_dAvpName[savpName])
            self.m_oDccAvp.clear()
        objList.reverse()
        self.m_tDccAvp = tuple(objList)

    def EN_Format2DccStream(self,fmtStr,dictData):
        """Get Dcc Stream.
        Cost me a lot of time.
        """
        def EN_GetAvpList(fmtStr,level,avpEnList,avpSqoList):
            """Put the PDCCTREE like:
            [427;;;],[873;;;]([874;;;]([865;;;]([866;;;],[2;;;])))
            To Dcc Stram like:
            [[427],[866,2,865,874,873],...]
            Reverse the complex avp list to ENCODE

            avpEnList: avp list decode from fmtStr.
            avpSqoList: the relationship of each avp,like father,child.
            level: the location of avp,like son,grandson.
            """
            fmtStr = fmtStr.replace(')[','),[')
            isub = level
            ibeg = 0
            iend = 0
            Flag = True
            while Flag:
                iend = fmtStr.find(',',ibeg)
                if iend == -1:
                    iend = len(fmtStr)
                    fmtStr += ','
                    Flag = False
                savp = fmtStr[ibeg:iend]
                (cure,m) = matchParentheses(savp,'[',']')
                cure += ibeg
                if m != '':
                    avpEnList.append(m)
                    avpSqoList.append(isub)
                if cure == iend-1:
                    ibeg = iend + 1
                else:
                    pStr = fmtStr[cure+1:]
                    (c,m) = matchParentheses(pStr,'(',')')
                    if m == '':
                        break
                    n = isub+1
                    EN_GetAvpList(m,n,avpEnList,avpSqoList)
                    ibeg = (c + 1) + cure
            return 0

        level = 0
        avpEnList =[]
        avpSqoList = []
        EN_GetAvpList(fmtStr,level,avpEnList,avpSqoList)
        self.encodeAvpintoTuple(avpEnList,avpSqoList,dictData)

    def genDccStream(self,pkgFmtStr,xdrValueDict,command):
        """Get dcc stream msg. """
        self.EN_Format2DccStream(pkgFmtStr,xdrValueDict)
        DccBody = ''
        BodyLen = 0
        DccLen = 0
        for avpobj in self.m_tDccAvp:
            sHead = ''
            sAvp = ''
            sHead = avpobj.encodeAvpHead()
            sAvp = avpobj.m_sValue
            DccBody += sHead + sAvp
            if avpobj.m_bParentFlag == 1:
                BodyLen += avpobj.m_iLength
                # avpobj.display()
        DccLen = DCC_HEAD_LEN/2 + BodyLen
        dccHeadList =[1,DccLen,1,0,0,0,command,23456,123456,321431]
        self.m_oDccHead.setHead(dccHeadList)
        DccHead = self.m_oDccHead.encode()

        return DccHead + DccBody