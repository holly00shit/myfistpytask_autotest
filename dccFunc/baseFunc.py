# -*- coding: utf-8 -*-
#!/usr/bin/env python

import os.path
from string import upper
import logging
import binascii
import sys

def atServerLog(logname,logconfig):
    """Construct a new 'Base' logging object and return it.
    Notice: Log Level should be set to 'Logger' not 'Hanlder'
    """
    if os.path.exists(logconfig[0]):
        pathFileName = os.path.join(logconfig[0],logconfig[1])
    else:
        print 'Can not find the path :',logconfig[0]
        return -1
    loglevel = upper(logconfig[2])
    logger = logging.getLogger(logname)
    if loglevel == 'WARN':
        logger.setLevel(logging.WARN)
    elif loglevel == 'INFO':
        logger.setLevel(logging.INFO)
    else:
        logger.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(name)-12s %(asctime)s %(levelname)-8s %(message)s',
                '%a, %d %b %Y %H:%M:%S',)
    file_handler = logging.FileHandler(pathFileName)
    file_handler.setFormatter(formatter)
    stream_handler = logging.StreamHandler(sys.stderr)
#    file_handler.setLevel(logging.DEBUG) # this is wrong !!!
    logger.addHandler(file_handler)
    logger.addHandler(stream_handler)

    return logger

def DecodeAvpValue(avpobj):
    """Decode avp'value according to Standard file of CMCC
     TO BE CONTINUE...
    """
    try:
        sValue = avpobj.m_sValue
        if avpobj.m_iAddLength:
            sValue = avpobj.m_sValue[:avpobj.m_iAddLength*(-2)]
        sDataType = avpobj.m_sDataType
        iLength = avpobj.m_iLength
    except AttributeError,e:
        print 'only to decode avp obj\n',e
        return 0

    if sValue == '':
        return 0
    if sDataType in ('Integer32','Integer64','Unsigned32','Unsigned64'):
        avpobj.m_sValue = int(sValue,16)
        if sDataType in ('Integer32','Unsigned32') and  iLength != 12 or \
           sDataType in ('Integer64','Unsigned64') and iLength != 16:
            print "standard uncatched !!! happened at AVP :",avpobj.m_iCode
        return 1
    elif sDataType in ('Float32','Float64'):
        return 1
    elif sDataType in ('UTF8String','OctetString','DiamIdent'):
        avpobj.m_sValue = binascii.a2b_hex(sValue)
        return 1
    elif sDataType == 'Address':
        return 1
    elif sDataType == 'Time':
        return 1
    elif sDataType == 'Enumerated':
        avpobj.m_sValue = int(sValue,16)
        return 1
    elif sDataType == 'Grouped':
        return 1

def EncodeAvpValue(avpobj):
    try:
        sValue = avpobj.m_sValue
        sDataType = avpobj.m_sDataType
        iLength = 0
    except AttributeError,e:
        print 'only to decode avp obj\n',e
        return 0

    if sValue == '':
        return 0
    if sDataType in ('Integer32','Integer64','Unsigned32','Unsigned64','Enumerated'):
        if sDataType in ('Integer32','Unsigned32','Enumerated'):
            iLength = 12 - avpobj.getAvpHeadLen()
        elif sDataType in ('Integer64','Unsigned64'):
            iLength = 16 - avpobj.getAvpHeadLen()
        try:
            sValue = int(avpobj.m_sValue)
        except:
            sValue = 0
        sValue = upper(hex(sValue)[2:])
        sValue = (iLength*2 - len(sValue))*'0' + sValue
        avpobj.m_sValue = sValue
        return 1
    elif sDataType in ('Float32','Float64'):
        return 1
    elif sDataType in ('UTF8String','OctetString','DiamIdent'):
        sValue = upper(binascii.b2a_hex(avpobj.m_sValue))
        iLength = len(sValue)/2
        if iLength%4 != 0:
            sValue += (4 - iLength%4)*'00'
        avpobj.m_sValue = sValue
        return 1
    elif sDataType == 'Address':
        return 1
    elif sDataType == 'Time':
        return 1
    elif sDataType == 'Grouped':
        return 1

def AvpCode2Value(pDccTreeObj,avpCodeValueDict):
    """Parase PDCCTREE object and
    Return a Map of {avp_code:value}
    """
    for avpObj in pDccTreeObj.m_tDccAvp:
        DecodeAvpValue(avpObj)
        avpCodeValueDict[avpObj.m_iCode] = avpObj.m_sValue

def AvpCode2XdrName(fmtStr,fmtDict):
    """Return a MAP of {avp_code:xdr_name,xdr_name:avp_code} according to
    pkgformat define in xxxcfg.py
    fmtStr:
        '[268;;;;;;],[264;;;;;;],[431;;;0;0;]([420;;;;;])
        [430;;;0;0;]([449;;;;;]([873;;;0;0;]([20300;;;0;0;]([613;;;;;]))'
    """
    pbeg = 0
    pend = 0
    avpList =[]
    t_fmtStr = fmtStr
    while pbeg < len(fmtStr):
        pStr = ''
        pbeg = t_fmtStr.find('[',pend)
        if pbeg == -1:
            break
        pend = t_fmtStr.find(']',pbeg)
        pStr = t_fmtStr[pbeg+1:pend]
        avpList.append(pStr)
        pbeg = pend
    for i in range(0,len(avpList)):
        avpList[i] = avpList[i].split(';')
        if avpList[i][5] == '':
            continue
        fmtDict[int(avpList[i][0])] = avpList[i][5].strip()

def matchParentheses(dataStr, leftF, rightF):
    """Match leftF and rightF,
    Return Str of leftF to rightF in dataStr ,
    The num of rightF in dataStr.
    """
    if len(dataStr) <= 0:
        return (0,None)
    ibeg = 0
    iend = 0
    n = 0
    for item in range(len(dataStr)):
        if dataStr[item] == leftF:
            n += 1
            if n ==1:
                ibeg = item
        elif dataStr[item] == rightF:
            n -= 1
            if n == 0:
                iend = item
                break
    return (iend,dataStr[ibeg+1:iend])







