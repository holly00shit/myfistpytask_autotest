# -*- coding: utf-8 -*-
#!/usr/bin/env python

#############################################
# Author : zhongks@aisainfo-linkage.com
# Date   : 2013/01/24
# Brief  : implenment class CDCCAvp
#############################################

from string import upper
from dccFunc.dccDefine import *

class PDCCAVP:
    """To store all the avp body.
    Notice that the parent of Grouped avp
    just have a head but a complete avp body.

    If modify __init__, do not forget to modify dccTree.copyPDccAvp()
    """
    def __init__(self):
        self.m_iCode = 0 # AVP代码
        self.m_iVendorId = 0 # 制造商ID,IETF定义的添0
        self.m_bFlagsV = False # AVP标记的V比特位
        self.m_bFlagsM = False # AVP标记的M比特位
        self.m_bFlagsP = False # AVP标记的P比特位
        self.m_iLength = 0 # AVP数据字节数
        self.m_iAddLength = 0 # AVP补'00'的字节数
        self.m_sDataType = '' # AVP数据格式
        self.m_sName = '' # AVP名字
        self.m_bOriMsg = '' # 原始报文
        self.m_sValue = '' # AVP数据,具体数据由客户端负责维护和解码
        self.m_sNext = '' # 下一个AVP,和当前AVP在同一层上
        self.m_bParentFlag = '' # 当前AVP父AVP,第一层AVP填NULL
        self.m_sSub = '' # 当前AVP的子AVP,当AVP数据类型为Grouped时存在,没有添NULL

    def clear(self):
        self.m_iCode = 0
        self.m_iVendorId = 0
        self.m_bFlagsV = False
        self.m_bFlagsM = False
        self.m_bFlagsP = False
        self.m_iLength = 0
        self.m_iAddLength = 0
        self.m_sDataType = ''
        self.m_sName = ''
        self.m_bOriMsg = ''
        self.m_sValue = ''
        self.m_sNext = ''
        self.m_sParents = ''
        self.m_sSub = ''

    def insertAvpMap(self):pass

    def clearAvpMap(self):pass

    def display(self):
        '显示Avp'
        print '========AVP=========='
        print 'm_iCode      :' , self.m_iCode
        print 'm_iVendorId  :' , self.m_iVendorId
        print 'm_bFlagsV    :' , self.m_bFlagsV
        print 'm_bFlagsM    :' , self.m_bFlagsM
        print 'm_bFlagsP    :' , self.m_bFlagsP
        print 'm_iLength    :' , self.m_iLength
        print 'm_sDataType  :' , self.m_sDataType
        print 'm_sName      :' , self.m_sName
        print 'm_sValue     :' , self.m_sValue
        print 'm_sNext      :' , self.m_sNext
        print 'm_bParentFlag   :' , self.m_bParentFlag
        print 'm_sSub       :' , self.m_sSub
        print 'm_bOriMsg    :' , self.m_bOriMsg
        print '\n'

    def decode(self,pDccAvplist):
        """
        pDccAvplist = [报文]
        pDccAvp 去掉dcc报文头的字符串
        """
        pDccAvp = pDccAvplist[0] # 参数为可变的list，转成str处理
        if pDccAvp == '':return IN_PARAM_ERR

        # avp head 解码 beg
        self.m_iCode = int(pDccAvp[0:AVP_CODE_LEN],16)
        pDccAvp = pDccAvp[AVP_CODE_LEN:]

        btFlag = int(pDccAvp[0:AVP_FLAGS_LEN],16)
        self.m_bFlagsV = ((btFlag&128)==128)
        self.m_bFlagsM = ((btFlag&64)==64)
        self.m_bFlagsP = ((btFlag&32)==32)
        pDccAvp = pDccAvp[AVP_FLAGS_LEN:]

        self.m_iLength = int(pDccAvp[0:AVP_LENGTH_LEN],16)
        pDccAvp = pDccAvp[AVP_LENGTH_LEN:]

        if self.m_bFlagsV:
            self.m_iVendorId = pDccAvp[0:AVP_VENDOR_ID_LEN]
            pDccAvp = pDccAvp[AVP_VENDOR_ID_LEN:]
        else:
            self.m_iVendorId = 0

        self.m_bOriMsg = pDccAvplist[0][0:self.getAvpHeadLen()*2]   #保存原始avp头
        # avp head 解码 end

        IgnoreFlag = False
        try:
            self.m_sDataType = AvpCode_Type_Map[self.m_iCode]
            self.m_sName = AvpCode_Name_Map[self.m_iCode]
        except KeyError,e:
            if AvpCode_Ignore_Map.has_key(self.m_iCode):
                IgnoreFlag = True
                print 'ignore avp code: ',self.m_iCode
            else:
                print 'can not recognise avp_code' ,self.m_iCode
                print e
                return UN_RECOGNISE_AVP_CODE

        if upper(self.m_sDataType) != 'GROUPED' and not IgnoreFlag:
            self.m_sValue = pDccAvp[0:(self.m_iLength-self.getAvpHeadLen())*2]
            pDccAvp = pDccAvp[(self.m_iLength-self.getAvpHeadLen())*2:]
            if pDccAvp == '':
                # print 'end of dcode !!!'
                return 0
            else:
                pass
            # avp长度不是4的倍数则补0
            m_iuu = self.m_iLength%4
            if m_iuu!= 0:
                self.m_iLength += (4-m_iuu)
                self.m_sValue += '00'*(4-m_iuu)
                pDccAvp = pDccAvp[m_iuu*2:]
                self.m_iAddLength = 4-m_iuu

        # 复合avp解码
        elif upper(self.m_sDataType) == 'GROUPED' and not IgnoreFlag:
            self.m_sValue = ''
            pDccAvplist[0] = pDccAvplist[0][self.getAvpHeadLen()*2:]
            return AVP_GROUPED
        # 要忽略的avp_code，直接跳过
        elif IgnoreFlag:
            pDccAvp = pDccAvplist[0][self.m_iLength*2:]

        pDccAvplist[0] = pDccAvp
        return 0

    def dump(self):pass

    def encodeAvpHead(self):
        """Turn avpobj Head to HEX STR. """
        avpHexStr = ''
        codeHex = upper(hex(self.m_iCode)[2:])
        codeHex = (AVP_CODE_LEN-len(codeHex))*'0' + codeHex
        vendorIdHex = upper(hex(self.m_iVendorId)[2:])
        vendorIdHex+= (AVP_VENDOR_ID_LEN-len(vendorIdHex))*'0' + vendorIdHex
        sFlag = ''
        if self.m_bFlagsV:
            sFlag = '80'
        else:
            sFlag = '40'
        lengthHex = upper(hex(self.m_iLength)[2:])
        lengthHex = (AVP_LENGTH_LEN-len(lengthHex))*'0' + lengthHex
        if self.m_bFlagsV:
            avpHexStr = codeHex + sFlag + lengthHex + vendorIdHex
        else:
            avpHexStr = codeHex + sFlag + lengthHex
        return avpHexStr

    def getAvpHeadLen(self):
        '取AVP头长度'
        if self.m_bFlagsV:
            return AVP_HEAD_V_LEN
        else:
            return AVP_HEAD_LEN

    def decodeGrouped(self,pDccAvpGroupedlist,iAvpGroupedLen):
        '''解码Grouped类型AVP'''
        if pDccAvpGroupedlist == []: return IN_PARAM_ERR
        if iAvpGroupedLen == 0: return 0                            #只有avp头，没有data
        if iAvpGroupedLen < AVP_HEAD_LEN or iAvpGroupedLen%4 != 0: return DIAMETER_INVALID_AVP_LENGTH

        iRet = self.decode(pDccAvpGroupedlist)
        return iRet

    def encodeGrouped(self):
        '编码Grouped类型AVP'
        pass

    def setAvp(self,avplist):
        self.m_iCode = avplist[0]
        self.m_iVendorId = avplist[1]
        self.m_bFlagsV = avplist[2]
        self.m_bFlagsM = avplist[3]
        self.m_bFlagsP = avplist[4]
        self.m_iLength = avplist[5]
        self.m_sName = avplist[6]
        self.m_sValue = str(avplist[7])
        self.m_sDataType = AvpCode_Type_Map[self.m_iCode]
