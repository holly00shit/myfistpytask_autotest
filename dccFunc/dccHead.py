# -*- coding: utf-8 -*-
#!/usr/bin/env python

#############################################
# Author : zhongks@aisainfo-linkage.com
# Date   : 2013/01/24
# Brief  : implenment class CDCCHead
#############################################
from string import upper

from dccDefine import *

#DCC报文头
class PDCCHEAD:
    """To store the 20byte long head of dcc massege. """
    def __init__(self):
        self.m_iVersion = 0
        self.m_iLength = DCC_HEAD_LEN
        self.m_bFlagsR = False
        self.m_bFlagsP = False
        self.m_bFlagsE = False
        self.m_bFlagsT = False
        self.m_iCode = 0
        self.m_iAppID = 0
        self.m_iHopIdentifier = 0
        self.m_iEndIdentifier = 0
        self.m_sOriHead = ''
        self.m_sAvp = ''

    def display(self):
        print 'm_iVersion:'.ljust(30),self.m_iVersion
        print 'm_iLength:'.ljust(30),self.m_iLength
        print 'm_bFlagsR:'.ljust(30),self.m_bFlagsR
        print 'm_bFlagsP:'.ljust(30),self.m_bFlagsP
        print 'm_bFlagsE:'.ljust(30),self.m_bFlagsE
        print 'm_iCode:'.ljust(30),self.m_iCode
        print 'm_iAppID:'.ljust(30),self.m_iAppID
        print 'm_iHopIdentifier:'.ljust(30),self.m_iHopIdentifier
        print 'm_iEndIdentifier:'.ljust(30),self.m_iEndIdentifier
        print 'm_sOriHead:'.ljust(30),self.m_sOriHead
        print 'm_sAvp:'.ljust(30),self.m_sAvp

    def dump(self,pDCCHead,szOut):
        iRet = self.decode(pDCCHead)
        szOut = 'version:%d  length:%d  RPET flag: %d%d%d%d  code:%d  app:%d  hop:%d  end:%d' \
                %(self.m_iVersion,self.m_iLength,self.m_bFlagsR,self.m_bFlagsP,
                  self.m_bFlagsE,self.m_bFlagsT, self.m_iCode,self.m_iAppID,
                  self.m_iHopIdentifier, self.m_iEndIdentifier)
        if 0 != iRet:
            szOut = 'decode head err %d' %(iRet)

    def decode(self,pDCCHead):
        """Decode Dcc head stream into dccheadobj'[PDCCHEAD]'. """
        if pDCCHead == '':
            return IN_PARAM_ERR

        self.m_sOriHead = pDCCHead[0:DCC_HEAD_LEN]

        self.m_iVersion = int(pDCCHead[0:DCC_VERSION_LEN],16)
        pDCCHead = pDCCHead[DCC_VERSION_LEN:]

        self.m_iLength = int(pDCCHead[0:DCC_LENGTH_LEN],16)
        pDCCHead = pDCCHead[DCC_LENGTH_LEN:]

        btFlag = 0
        btFlag = int(pDCCHead[0:DCC_FLAGS_LEN],16)
        self.m_bFlagsR = ((btFlag&128)==128)
        self.m_bFlagsP = ((btFlag&64)==64)
        self.m_bFlagsE = ((btFlag&32)==32)
        self.m_bFlagsT = ((btFlag&16)==16)
        pDCCHead = pDCCHead[DCC_FLAGS_LEN:]

        self.m_iCode = int(pDCCHead[0:DCC_CODE_LEN],16)
        pDCCHead = pDCCHead[DCC_CODE_LEN:]

        self.m_iAppID = int(pDCCHead[0:DCC_APPID_LEN],16)
        pDCCHead = pDCCHead[DCC_APPID_LEN:]

        self.m_iHopIdentifier = int(pDCCHead[0:DCC_HOPIDENT_LEN],16)
        pDCCHead = pDCCHead[DCC_HOPIDENT_LEN:]

        self.m_iEndIdentifier = int(pDCCHead[0:DCC_ENDIDENT_LEN],16)
        pDCCHead = pDCCHead[DCC_ENDIDENT_LEN:]

#        if len(pDCCHead)<DCC_HEAD_LEN or len(pDCCHead)%4 != 0:
#            return DIAMETER_INVALID_MESSAGE_LENGTH

        if self.m_bFlagsR == True and self.m_bFlagsE == True:
            return DIAMETER_INVALID_HDR_BITS

        return 0

    def encode(self):
        sHeadMsg = ''
        iVersion = hex(self.m_iVersion)[2:]
        iVersion = (DCC_VERSION_LEN - len(iVersion))*'0' + iVersion
#        sHeadMsg += iVersion
        iLength = upper(hex(self.m_iLength)[2:])
        iLength = (DCC_LENGTH_LEN - len(iLength))*'0' + iLength
        sFlag = ''
        if self.m_bFlagsR and not self.m_bFlagsP:
            sFlag = '80'
        elif self.m_bFlagsR and self.m_bFlagsP:
            sFlag = 'C0'
        else:
            print 'Not supported Flag of Dcc Head.'
        iCode = upper(hex(self.m_iCode)[2:])
        iCode = (DCC_CODE_LEN - len(iCode))*'0' + iCode
#        sHeadMsg += (DCC_CODE_LEN - len(iCode))*'0' + iCode
        iAppID = upper(hex(self.m_iAppID)[2:])
        iAppID = (DCC_APPID_LEN - len(iAppID))*'0' + iAppID
#        sHeadMsg += (DCC_APPID_LEN - len(iAppID))*'0' + iAppID
        iHop = upper(hex(self.m_iHopIdentifier)[2:])
        iHop = (DCC_HOPIDENT_LEN - len(iHop))*'0' + iHop
#        sHeadMsg += (DCC_HOPIDENT_LEN - len(iHop))*'0' + iHop
        iEnd = upper(hex(self.m_iEndIdentifier)[2:])
        iEnd = (DCC_ENDIDENT_LEN - len(iEnd))*'0' + iEnd
#        sHeadMsg += (DCC_ENDIDENT_LEN - len(iEnd))*'0' + iEnd
        sHeadMsg = iVersion + iLength + sFlag + iCode + iAppID + iHop + iEnd
        return sHeadMsg

    def setHead(self,dccheadlist):
        """Dcc Head Length to do. """
        self.m_iVersion = dccheadlist[0]
        self.m_iLength = dccheadlist[1]
        self.m_bFlagsR = dccheadlist[2]
        self.m_bFlagsP = dccheadlist[3]
        self.m_bFlagsE = dccheadlist[4]
        self.m_bFlagsT = dccheadlist[5]
        self.m_iCode = dccheadlist[6]
        self.m_iAppID = dccheadlist[7]
        self.m_iHopIdentifier = dccheadlist[8]
        self.m_iEndIdentifier = dccheadlist[9]



