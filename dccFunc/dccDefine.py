# -*- coding: utf-8 -*-
#!/usr/bin/env python

import xml.etree.ElementTree as etree



DCC_HEAD_LEN=20   *2 #DCC头长度
DCC_VERSION_LEN=1 *2 #DCC版本号长度
DCC_LENGTH_LEN=3  *2 #DCC报文长度字段长度
DCC_FLAGS_LEN=1   *2 #DCC标志位长度
DCC_CODE_LEN=3    *2 #DCC 命令码长度
DCC_APPID_LEN=4   *2 #DCC app-id长度
DCC_HOPIDENT_LEN=4  *2 #DCC  Hop-by-Hop标识符长度
DCC_ENDIDENT_LEN=4  *2 #DCC  End-to-End标识符长度


AVP_HEAD_LEN=8       #AVP头（v标志不置位）长度
AVP_HEAD_V_LEN=12    #AVP头（v标志置位）长度
AVP_CODE_LEN=4    *2 #AVP code字段长度
AVP_FLAGS_LEN=1   *2 #AVP 标志位长度
AVP_LENGTH_LEN=3  *2 #AVP 长度字段长度
AVP_VENDOR_ID_LEN=4 *2  #AVP 制造商id长度

AVP_GROUPED=99
# AVP数据数据类型
GROUPED = 0
INTEGER32 = 1
INTEGER64 = 2
UNSIGNED32 =3 
UNSIGNED64 = 4
FLOAT32 = 5
FLOAT64 = 6
OCTET_STRING = 7
ADDRESS = 8
TIME = 9
UTF8_STRING = 10
DIAMETER_IDENTITY = 11
ENUMERATED  = 12

#err_code
IN_PARAM_ERR                  = -81
HEAD_IS_NULL                  = -82
AVP_IS_NULL                   = -83
DIAMETER_INVALID_MESSAGE_LENGTH         = -84
DIAMETER_INVALID_AVP_LENGTH           = -85
DIAMETER_INVALID_HDR_BITS             = -86
DIAMETER_AVP_UNSUPPROTED            = -87
DIAMETER_AVP_LACK               = -88
AVP_TOO_MANY                  = -89
UN_RECOGNISE_AVP_CODE          = -90


AvpCode_Type_Map = {}
AvpCode_Name_Map = {}
AvpCode_VendorId_Map={}

###初始化avp.cfg###
avp_define_cfg = open('avp.cfg','r')
for xmlline in avp_define_cfg.readlines():
    if xmlline.strip() != '':
        tree = etree.fromstring(xmlline)
        avp_code = int(tree.find('code').text)
        avp_type = tree.find('type').text
        avp_name = tree.find('name').text
        avp_vendorid = int(tree.find('vendorId').text)
        AvpCode_Type_Map[avp_code] = avp_type
        AvpCode_Name_Map[avp_code] = avp_name
        AvpCode_VendorId_Map[avp_code] = avp_vendorid
avp_define_cfg.close()
#print AvpCode_Type_Map


###定义ignore avp###
AvpCode_Ignore_Map = {1264:'Trigger'}

#dr_scp

#if __name__ == '__main__':
#    pass