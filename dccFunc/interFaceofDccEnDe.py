# -*- coding: utf-8 -*-
from dccFunc.baseFunc import AvpCode2XdrName, AvpCode2Value
from dccFunc.dccHead import PDCCHEAD
from dccFunc.dccTree import PDCCTREE

def PrtCdrDict(fmtStr,dataLen,byteData):
    """Decode byteData completely and
    Associate xdr_name with value.
    """
    avpCodeValueDict = {}
    fmtDict = {}
    XDRCODE_VALUE = {}
    pDccTree = PDCCTREE()
    pDccTree.decodeAvpintoTuple(dataLen,byteData)
    AvpCode2Value(pDccTree,avpCodeValueDict)
    AvpCode2XdrName(fmtStr,fmtDict)
    for i in fmtDict.keys():
        if avpCodeValueDict.has_key(i):
            XDRCODE_VALUE[fmtDict[i]] = avpCodeValueDict[i]
        else:
            XDRCODE_VALUE[fmtDict[i]] = ''
    return XDRCODE_VALUE


def GetDccMsg(pkgStr,dataDict,command):
    gsmDccTree = PDCCTREE()
    return gsmDccTree.genDccStream(pkgStr,dataDict,command)

def prtDcc(dataLen,databyte):
    return databyte[:dataLen]

def parseDccHead(headLen,headData):
    """Decode and return CommandCode and MsgLength
    """
    PDccHead = PDCCHEAD()
    PDccHead.decode(headData)
    return PDccHead.m_iCode,PDccHead.m_iLength









