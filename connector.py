#!/usr/bin/env python
#coding=gbk

from socket import *
import sys,os
import logging
from string import *
from dccFunc.interFaceofDccEnDe import prtDcc, parseDccHead

class connector(object):
    def __init__(self, hlog, destIP, destPORT):
        self.log = hlog
        self.ip = destIP
        self.port = int(destPORT)
        self.conn = None
        
    def send(self,strData):
        if self.conn == None and (not self.connectTo()):
           return False
            
        flag = True
        try:
            self.conn.sendall(strData)
        except error, e:
            self.log.info("when send data,socket error:%s." % e)
            print 'fail to communicate with server.'
            flag = False
        
        if not flag:
            self.reconnectTo()
            try:
               self.conn.sendall(strData)
            except error,e:
               self.log.info("retry,socket error:%s." % e)
               return False
        
        return True
        
    def connectTo(self):
        address = (self.ip, self.port)
        if self.ip == '' or self.port < 1024:
            self.log.error("host or port error:%s,%d" % self.ip, self.port)
            print "host or port error:%s,%d" % self.ip, self.port
            return False
            
        try:
            self.conn = socket(AF_INET, SOCK_STREAM)
            self.conn.connect(address)
        except error,e:
            self.log.error('Fail to connect to server: %s,detail: %s' % (str(address),str(e)))
            print 'Fail to connect to server: %s,detail: %s' % (str(address),str(e))
            self.conn.close()
            self.conn = None
            return False
        
        return True
        
    def reconnectTo(self):
        if self.conn != None:
            self.conn.close()
            self.conn = None
        
        self.connectTo()
    
    #result,message,length,command
    def recv(self):
        #self.log.error("Must implement the recv for the dest object")
        #sys.exit()
        if self.conn == None:
            return (1,'socket is null',0,0)

#        headLen = dccHeadlen()
        headLen = 20
        try:
            headData = self.conn.recv(headLen)
        except timeout,e:
            return (0,'TIMEOUT',0,0)
        except error,e :
            return (1,'socket maybe miss during recv head: %s ' % e,0,0)
        
        if not headData:
            return (1,'socket is the other closed',0,0)
        
        if len(headData) != headLen:
            return (1,'fail to recv head:%s' % prtDcc(len(headData),headData),0,0)
            
        command,pkgLen = parseDccHead(headLen,headData)
        
        bodyData = ''
        if pkgLen > 0:
            try :
                rnbytes = 0
                while rnbytes < pkgLen - headLen:
                    nsize = 0
                    recvbuffer = bytearray(pkgLen-rnbytes)
                    nsize = self.conn.recv_into(recvbuffer)
                    rnbytes += nsize
                    bodyData += str(recvbuffer[:nsize])
            except error,e:
                return (1,'socket error during recv body: %s ' % e,pkgLen,command)
        else:
            return (2,prtDcc(len(headData),headData),pkgLen,command)
        
        return (0,headData+bodyData,pkgLen,command)

