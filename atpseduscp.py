#!/usr/bin/env python
#coding=gbk

import sys,os
import traceback
import logging
from socket import *
import threading
from time import ctime
from string import *
from atpack import *
from configfile import ConfigFile
from dccFunc.baseFunc import atServerLog
from serverscp import *

class servershort(threading.Thread):
    def __init__(self, hsyslog,subSiglList,subContList):
        threading.Thread.__init__(self)
        self.syslog = hsyslog
        self.siglList = subSiglList
        self.contList = subContList
        
        self.cfgPool = None
        self.slock = threading.Lock()
        self.clock = threading.Lock()

    def __call__(self):
        st = threading.Thread(target = dealSiglTask(self.syslog,self.siglList,self.slock))
        st.start()
        
        ct = threading.Thread(target = dealContTask(self.syslog,self.contList,self.clock))
        ct.start()
        

class serverlong(threading.Thread):
    def __init__(self, cs,hsyslog,subSiglList,subContList):
        threading.Thread.__init__(self)
        self.tcpCliSock = cs
        self.syslog = hsyslog
        self.siglList = subSiglList
        self.contList = subContList
        
        self.cfgPool = None
        self.slock = threading.Lock()
        self.clock = threading.Lock()

    def __call__(self):
        gt = threading.Thread(target = getTask(self.tcpCliSock,self.syslog,self.siglList,self.contList))
        gt.start()

        st = threading.Thread(target = dealSiglTask(self.syslog,self.siglList,self.slock))
        st.start()
        
        ct = threading.Thread(target = dealContTask(self.syslog,self.contList,self.clock))
        ct.start()
        

def main():
    if len(sys.argv)<3 or sys.argv[1].lower()!="-i" or len(sys.argv[2])==0:
       print "Usage:%s -i configfile" %sys.argv[0]
       sys.exit()

    cfgfile=sys.argv[2]
    configfile = ConfigFile(cfgfile)

    threadnums = configfile.getMaxthreadFromCfg()

    hsyslog = atServerLog("atpseduscp", configfile.getSystemLogFromCfg()) # init log file
    hsyslog.info("main starting at: %s" % ctime())


    #key:client name,val:(psiglDataList,pcontDataList,psiglThread,pcontThread)
    dictConnt = {}
    #(seq,conn,dest_ip,dest_port,bussness,data_type,oper_type,org_data,state,mid_data)
    specSiglList = []
    specContList = []
            
    port = int(configfile.getServerPort())
    host = '127.0.0.1'
    address = (host, port)
    tcpSerSock = socket(AF_INET, SOCK_STREAM)
    tcpSerSock.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
    try:
        tcpSerSock.bind(address)
        tcpSerSock.listen(int(threadnums))
    except error,msg:
        tcpSerSock.close()
        hsyslog.error("fail to bind: %s" % str(msg))
        return

    #for short-connect client
    siglthread = threading.Thread(target = servershort(hsyslog,specSiglList,specContList))
    siglthread.start()
    
    while True:
        try:
            cs,address = tcpSerSock.accept()
        except:
            traceback.print_exc()
            continue
        #authentication and analyse
        errorMsg = ''
        relcode,recvData,ctlLen,msgType = recvAtpackage(cs)
        if relcode in (1,2):
            hsyslog.info('Error: recv data: %s' % recvData)
            hsyslog.info("fail to auth the connecter due to not recv request.")
            cs.close()
            #continue
        else:
            sendPack = AtMsgCtlPack()
            connType = -1
            rlt_code = True
            msg = ''

            if msgType != NOTIFY['REGISTER_R']:
                hsyslog.info('here hope a auth pkg,but recv: %s' % msgType)
                rlt_code,msg = answer(cs,'fail',NOTIFY['REGISTER_A'])
            else:
                hsyslog.info('DEBUG: recv Register data: %s' % recvData)
                retPack = AtMsgCtlPack()
                retPack.setPackData(recvData)
                clientName = retPack.getKey('name')
                connType = int(retPack.getKey('type'))
                
                hsyslog.info('auth:%s,%s' % (clientName,connType))
                rlt_code,msg = answer(cs,'success',NOTIFY['REGISTER_A'])
                
            if rlt_code == False:
                hsyslog.error("Fail to respone auth,error: %s" % msg)
                cs.close()
                continue

            if connType == 1:
               subSiglList = []
               subContList = []
               st = threading.Thread(target = serverlong(cs,hsyslog,subSiglList,subContList))
               st.start()
               dictConnt[clientName] = (subSiglList,subContList,st)
               
            elif connType == 0:
               gt = threading.Thread(target = getTask(cs,hsyslog,specSiglList,specContList))
               gt.start()
               #deal siglthread
            else:
                hsyslog.error("unknow connType: %s" % connType)

    hsyslog.info("main exit at: %s" % ctime())
     
if __name__ == '__main__':
      main()
