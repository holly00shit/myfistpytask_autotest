#!/usr/bin/env python
#coding=gbk

import datetime
import re
import time

class basecfg(object):
    def __init__(self):
        self.pkgFormt = {
            (280,'R'):"""
[264;enc_OctetString;;0;0;ORIGIN_HOST],
[296;enc_OctetString;;0;0;ORIGIN_REALM],
[278;enc_Unsigned32;;0;0;ORIGIN_STATE_ID]
""",
            (280,'A'):"""
[268;enc_Integer32;;0;0;ERR_CODE],
[264;enc_OctetString;;0;0;ORIGIN_HOST],
[296;enc_OctetString;;0;0;ORIGIN_REALM],
[281;enc_OctetString;;0;0;ERROR_MESSAGE]
""",
            (257,'R'):"""
[264;enc_OctetString;;0;0;ORIGIN_HOST],
[296;enc_OctetString;;0;0;ORIGIN_REALM],
[257;enc_OctetString;;0;0;HOST_IP_ADDRESS],
[266;enc_Unsigned32;;0;0;VENDOR_ID1],
[269;enc_OctetString;;0;0;PRODUCT_NAME]
""",
            (257,'A'):"""
[268;enc_Integer32;;0;0;ERR_CODE],
[264;enc_OctetString;;0;0;ORIGIN_HOST],
[296;enc_OctetString;;0;0;ORIGIN_REALM],
[257;enc_OctetString;;0;0;HOST_IP_ADDRESS],
[266;enc_Unsigned32;;0;0;VENDOR_ID1],
[269;enc_OctetString;;0;0;PRODUCT_NAME]
""",
            (258,'R'):"""
[263;enc_OctetString;;0;0;SESSION_ID],
[264;enc_OctetString;;0;0;DESTINATION_HOST],
[296;enc_OctetString;;0;0;DESTINATION_REALM],
[283;enc_OctetString;;0;0;ORIGIN_REALM],
[293;enc_OctetString;;0;0;ORIGIN_HOST],
[258;enc_Unsigned32;;0;0;AUTH_APPLICATION_ID],
[285;enc_Unsigned32;;0;0;RE_AUTH_REQUEST_TYPE]
""",
            (258,'A'):"""
[263;enc_OctetString;;0;0;SESSION_ID]
"""
        }
    
    #[0;1;2;3;4;xdrname;6]
    def upperXdrname(self,fmtstr):
        rltstr = ''
        ibeg = 0
        iend = len(fmtstr)
        while ibeg < iend:
            ipos = fmtstr.find('[',ibeg)
            if ipos >= 0:
                rltstr += fmtstr[ibeg:ipos+1]
                ibeg = ipos + 1
                ipos = fmtstr.find(']',ibeg)
                tl = fmtstr[ibeg:ipos].split(';')
                if len(tl) >= 6:
                    tl[5] = tl[5].upper()           #将第五个值大写
                rltstr += ';'.join(tl)
            else:
                rltstr += fmtstr[ibeg:iend]
                ipos = iend
            ibeg = ipos
        return rltstr
        
    #user:12345678;
    def dict2FV(self,dictData):
        #if not isinstance(dictData,dict):
        #    return None
            
        result = ''
        for strKey in dictData.keys():
            if strKey[:3] == '##_':
                result += strKey + '{' + dictData[strKey] + '};'
            else:
                result += strKey + ':' + str(dictData[strKey]) + ';'
        
        return result

    #yyyymmddhhmissnnnnnn
    def genSessionID(self):
        return datetime.datetime.now().strftime("%Y%m%d%H%M%S%f")

    @classmethod
    def get_strip_time(self,start_time):
        """Turn yyyymmddHHMMSS to Timestamp.
        """
        mx = re.match(r'\d{14}',start_time)
        if mx == '':
            print 'start_time can not be recognized :%s' %start_time
            return -1
        return int(time.mktime(time.strptime(mx,'%Y%m%d%H%M%S')))

        
        