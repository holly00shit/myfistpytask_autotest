#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import os
from string import upper
import xml.etree.ElementTree as etree
import sys


class ConfigFile:
    """read the config file and pares it.
    """
    def __init__(self,configfile):
        try:
            self.Treeconfig = etree.parse(configfile)
        except Exception,e:
            print "Error: cannot parse file: %s" %configfile

    def get_root(self):
        return self.Treeconfig.getroot()

    def getMaxthreadFromCfg(self):
        """Get Max num of thread.
        """
        try:
            return self.Treeconfig.find('common/maxthreads').text
        except:
            return 0

    def getServerPort(self):
        """No Comment.
         """
        try:
            return self.Treeconfig.find('common/serverport').text
        except:
            return 0

    def getSystemLogFromCfg(self):
        """Get User's define of system log.
        """
        path = self.Treeconfig.find('system_log/path').text
        logfile = self.Treeconfig.find('system_log/log_prefix').text
        loglevel = self.Treeconfig.find('system_log/loglevel').text
        return [path,logfile,loglevel]

    def getCdrFileFromCfg(self):
        """Get cdr file
        """
        try:
            return self.Treeconfig.find('common/cdrfile').text
        except:
            return ''

    def getItem(self,tagname):
        """No Comment. """
        return self.Treeconfig.find(tagname).text

    def initClientItem(self):
        self.servHost = self.getItem("common/serverip")
        self.servPort = int(self.getItem("common/serverport"))
        self.myName = self.getItem("common/clientname")
        self.temp_ct = self.getItem("common/conntype")
        self.inbossdHost = self.getItem("common/inbossdip")
        self.inbossdPort = self.getItem("common/inbossdport")
        self.bussness = self.getItem("common/busstype").upper()
        self.pkgModel = self.getItem("common/pkgmodel")
        self.cdrFile = self.getItem("common/cdrfile")
        self.datatype = self.getItem("common/datatype")
        self.splitFlag = self.getItem("common/cdrsplit")


def atServerLog(logname,logconfig):
    """Construct a new 'Base' logging object and return it.
    Notice: Log Level should be set to 'Logger' not 'Hanlder'
    """
    if os.path.exists(logconfig[0]):
        pathFileName = os.path.join(logconfig[0],logconfig[1])
    else:
        print 'Can not find the path :',logconfig[0]
        return -1
    loglevel = upper(logconfig[2])
    logger = logging.getLogger(logname)
    if loglevel == 'WARN':
        logger.setLevel(logging.WARN)
    elif loglevel == 'INFO':
        logger.setLevel(logging.INFO)
    else:
        logger.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(name)-12s %(asctime)s %(levelname)-8s %(message)s',
                                  '%a, %d %b %Y %H:%M:%S',)
    file_handler = logging.FileHandler(pathFileName)
    file_handler.setFormatter(formatter)
    stream_handler = logging.StreamHandler(sys.stderr)
    #    file_handler.setLevel(logging.DEBUG) # this is wrong !!!
    logger.addHandler(file_handler)
    logger.addHandler(stream_handler)

    return logger
