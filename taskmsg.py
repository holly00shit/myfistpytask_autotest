#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import datetime
from configfile import ConfigFile
from atpack import AtMsgCtlPack, packageHead, NOTIFY, DCCTYPE, CCREQTYPE, recvAtpackage
from convertsth import  parase_user_input, connect_value_to_name, get_dict_to_fv


class TaskMsg():
    """Send task msg to server
    """
    def __init__(self,cs,seq,command,configfile):
        """Init
        """
        self.command = command
        self.configfile = configfile
        self.opertype = DCCTYPE['ALL']
        self.seq = seq
        self.cs = cs
        self.business = configfile.bussness.upper()
        self.seesion_id = ''

    def deal_xml_to_fvpair(self):
        """Convert xml of cdr to FVpair(f:v)
        Here are some special deals. Set value depend on user input.
        Param In :
             cdrxml : user_selected xml obj of cdr
             in_str : user input/A/I/U/T/rg()
        Param Out:
             fvstr(f:v;f:v...)
        """
        cdrxml = self.cdrxml
        in_str = self.in_str
        xdr_tags = []
        rg_value_list = []
        cc_req_type,rg_names,spec_dict = parase_user_input(in_str)
        xdr_tags.append(cdrxml.find('base'))
        extxmlobj = cdrxml.find('ext')
        if extxmlobj != None:
            xdr_tags.append(extxmlobj)
        # print 'rg_names: ',rg_names

        if cdrxml.find('rgs') is not None:
            for rg_name in rg_names:
                rg_val = cdrxml.find('rgs').find(rg_name).text.strip().split(';')
                # special deal
                if spec_dict.has_key(rg_name):
                    rg_val = rg_val[0:1] + [spec_dict[rg_name]]
                    rg_val.insert(0,rg_name)
                rg_value_list.append(rg_val)

        cdr_dict = connect_value_to_name(xdr_tags)
        # special deal
        cdr_dict['PROCESS_TYPE'] = CCREQTYPE[cc_req_type]
        if 'SESSION_ID' not in cdr_dict.keys() and self.seesion_id == '':
            cdr_dict['SESSION_ID'] = self.genSessionID()
            self.seesion_id = cdr_dict['SESSION_ID']
        else:
            cdr_dict['SESSION_ID'] = self.seesion_id

        if cc_req_type == 'A':
            self.opertype = '0'
        else:
            self.opertype = '1'
        if cc_req_type in spec_dict.keys() \
            and spec_dict[cc_req_type] != '':
            cdr_dict['IN_BUDGET_RES'] = spec_dict[cc_req_type]


        cdr_base_ext_str,rg_str = get_dict_to_fv(cdr_dict,rg_value_list)
        if self.business == 'GPRS':
            rg_str =';' + '##_2{' + rg_str +'}'
        else:
            rg_str = ''

        return cdr_base_ext_str+rg_str

    def __get_xml_data(self):
        """Put head data to xml
        data:
        """
        vals = self.deal_xml_to_fvpair()
        headInfo = (self.seq,self.configfile.inbossdHost,self.configfile.inbossdPort,
                    self.configfile.bussness,self.configfile.datatype,self.command,self.opertype)
        taskPack = AtMsgCtlPack()
        self.__setTaskPkgReq(taskPack,headInfo)
        taskPack.setKey('data',vals)
        xmlData = taskPack.getPackData()
        return xmlData

    def __get_head_msg(self):
        """Head of msg seded to server
        """
        xmlData = self.__get_xml_data()
        if xmlData == -1:
            return xmlData
        headData = packageHead(len(xmlData),NOTIFY['TASK_R'])
        print 'headData : ',headData+xmlData
        return headData+xmlData

    @staticmethod
    def readcdrfile(cfg):
        """Init old cdr file to cdrlist.
        one cdr one line
        """
        cdrfile = cfg
        if not os.path.isfile(cdrfile):
            print 'Fail to read cdrfile:%s is not a file' % cdrfile
            return ''
        try:
            f = open(cdrfile,'r')
        except IOError,e:
            print str(e)
            return ''
        data = f.readlines()
        f.close()
        cdrlist = []
        for line in data:
            if line.strip() == '' or line.startswith('#'):
                continue
            cdrlist.append(line)
        return cdrlist

    @staticmethod
    def read_new_cdr_file(cfg):
        """cdr of xml
        one cdr one record.
        """
        cdr_cfg = ConfigFile(cfg)
        root = cdr_cfg.get_root()
        return root

    @classmethod
    def check_input(self,instr,count):
        """To ensure the input num correct!!
        """
        if instr.strip() == '':
            return -1
        try:
            innum = int(instr)
        except ValueError,e:
            print "unexpect input!!"
            return -1
        if innum < 0 or innum > count:
            print "out of range !!"
            return -1
        return innum

    def __setTaskPkgReq(self,taskPack,tupleHead):
        """authentication pkg
        """
        if len(tupleHead) < 7:
            return False

        taskPack.setKey('seq',tupleHead[0])
        taskPack.setKey('desthost',tupleHead[1])
        taskPack.setKey('destport',tupleHead[2])
        taskPack.setKey('bussness',tupleHead[3])
        taskPack.setKey('datatype',tupleHead[4])
        taskPack.setKey('command',tupleHead[5])
        taskPack.setKey('opertype',tupleHead[6])

        return True

    def send_task_msg(self,cdrxml,in_str):
        """send msg to server
        """
        self.cdrxml = cdrxml
        self.in_str = in_str
        try:
            head_msg = self.__get_head_msg()
            if head_msg == -1:
                return -1
            self.cs.sendall(head_msg)
        except IOError, e:
            print str(e)
            return -1
        return 1

    def receive_server_msg(self):
        """Receive from server
        """
        isEnd = '0'
        while isEnd == '0':
            relcode,recvData,ctlLen,msgType = recvAtpackage(self.cs)
            print "receive from server:%s" %recvData
            if relcode in (1,2):
               print 'fail to task'
               print recvData
               self.cs.close()
            else:
               if msgType != NOTIFY['TASK_A']:
                  print 'recv error answer'
                  self.cs.close()

               retPack = AtMsgCtlPack()
               retPack.setPackData(recvData)
               rltseq = retPack.getKey('seq')
               isEnd = retPack.getKey('isend')
               if rltseq != self.seq:
                   print 'answer not request :%s,%s' % (rltseq,self.seq)
               else:
                   result = retPack.getKey('result')
                   if result != 'success':
                      print retPack.getKey('msg')
                      self.cs.close()
                   else:
                      print retPack.getKey('data')
               if isEnd == '1':
                   print 'task shuld end!!!'
                   return 1

               if not self.in_str.startswith('A'):
                   return 0

        return 0

    def genSessionID(self):
        return datetime.datetime.now().strftime("%Y%m%d%H%M%S%f")
