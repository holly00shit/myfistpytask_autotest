#!/usr/bin/env python
#coding=gbk

import sys,os
import logging
import time
from basecfg import *
from connector import *
from atpack import DCCTYPE
from dccFunc.dccTree import PDCCTREE
from dccFunc.interFaceofDccEnDe import GetDccMsg, PrtCdrDict, prtDcc
from dcclib import *

class gsmcfg(basecfg):
    def __init__(self, hlog, destIP, destPORT):
        basecfg.__init__(self)
        self.conn = connector(hlog, destIP, destPORT)
        if not self.conn.connectTo():
            self.conn = None
            
        #logflag = 0 dcc stream; 1 dump dcc detail; 2 cdr str
        self.logflag = 2
        self.timerate = 60
        
        basefmt = """
[263;enc_OctetString;;0;0;SESSION_ID;1],
[264;enc_OctetString;;0;0;ORIGIN_HOST;scp172],
[296;enc_OctetString;;0;0;ORIGIN_REALM;scp172],
[283;enc_OctetString;;0;0;DESTINATION_REALM;scp172],
[258;enc_Unsigned32;;0;0;AUTH_APPLICATION_ID;4],
[461;enc_OctetString;;0;0;Service_ID;in@cmcc.com],
[415;enc_Unsigned32;;0;0;CC_REQUEST_NUMBER;1],
[293;enc_OctetString;;0;0;Destination_Host;scp172],
[55;enc_UTCTime;;0;0;START_TIME],
"""
        updtfmt = """
[416;enc_Unsigned32;;0;0;PROCESS_TYPE],
[446;;;0;0;]([420;enc_Unsigned32;;0;0;ADDUP_RES])
"""
        termfmt = """
[416;enc_Unsigned32;;0;0;PROCESS_TYPE],
[446;;;0;0;]([420;enc_Unsigned32;;0;0;DURATION])
"""
        initfmt = """
[416;enc_Unsigned32;;0;0;PROCESS_TYPE;1],
[873;;;0;0;]([20300;;;0;0;]([601;enc_OctetString;;0;0;MSC_ID],
                            [20339;enc_Unsigned32;;0;0;CALL_TYPE]
"""
        mocfmt = """
             [603;enc_OctetString;;0;0;USER_NUMBER],
             [20327;enc_OctetString;;0;0;OPP_NUMBER],
             [606;enc_OctetString;;0;0;MSRN],
             [607;enc_OctetString;;0;0;LAC_CELL]
             ))
"""
        mtcfmt = """
             [603;enc_OctetString;;0;0;OPP_NUMBER],
             [20327;enc_OctetString;;0;0;USER_NUMBER],
             [20387;enc_Unsigned32;;0;0;CHARGE_FLAG],
             [606;enc_OctetString;;0;0;MSRN],
             [608;enc_OctetString;;0;0;LAC_CELL]
             ))
"""
        mfcfmt = """
             [603;enc_OctetString;;0;0;A_NUMBER],
             [20327;enc_OctetString;;0;0;OPP_NUMBER],
             [606;enc_OctetString;;0;0;MSRN],
             [607;enc_OctetString;;0;0;LAC_CELL],
             [609;enc_OctetString;;0;0;USER_NUMBER],
             [611;enc_OctetString;;0;0;REDIRECTION_COUNTER],
             [612;enc_OctetString;;0;0;Redirection-Reason]
             ))
"""
        respfmt = """
[263;dec_OctetString;;0;0;SESSION_ID],
[264;dec_OctetString;;0;0;DESTINATION_HOST;OCS001.cmcc.com],
[268;dec_Unsigned32;;0;0;ERR_CODE],
[296;dec_OctetString;;0;0;DESTINATION_REALM;cmcc.com],
[258;dec_Unsigned32;;0;0;AUTH_APPLICATION_ID],
[416;dec_Unsigned32;;0;0;PROCESS_TYPE],
[415;dec_Unsigned32;;0;0;CC_REQUEST_NUMBER],
[431;;;0;0;]([420;dec_Unsigned32;;0;0;IN_BUDGET_RES])
[430;;;0;0;]([449;dec_Unsigned32;;0;0;;0])
[873;;;0;0;]([20300;;;0;0;]([613;dec_Unsigned32;;0;0;CHARGE_FLAG;1]))
"""

        self.pkgFormt[(272,'RIO')] = self.upperXdrname(''.join((basefmt+initfmt+mocfmt).split()))
        self.pkgFormt[(272,'RIT')] = self.upperXdrname(''.join((basefmt+initfmt+mtcfmt).split()))
        self.pkgFormt[(272,'RIF')] = self.upperXdrname(''.join((basefmt+initfmt+mfcfmt).split()))
        self.pkgFormt[(272,'RU')] = self.upperXdrname(''.join((basefmt+updtfmt).split()))
        self.pkgFormt[(272,'RT')] = self.upperXdrname(''.join((basefmt+termfmt).split()))
        self.pkgFormt[(272,'A')] = self.upperXdrname(''.join(respfmt.split()))
        
    def convertLacCell(self,lac,cell):
        #MCC(480)MNC(00)LAC(00000)CI(00000)
        tlac = int(eval('0x'+lac))
        tcell = int(eval('0x'+cell))
        
        return '48000%05d%05d' % (tlac,tcell)

    def analyse(self,dictData):
        """For Init Message.
        Add some infomation which is Necessary.
        """
        if 'START_TIME' in dictData.keys() and \
          len(dictData['START_TIME']) > 0:
            pass
        else:
            dictData['START_TIME'] = time.strftime("%Y%m%d%H%M%S",time.localtime())
        
        dictData['CC_REQUEST_NUMBER'] = 1
        dictData['PROCESS_TYPE'] = 1
        dictData['ADDUP_RES'] = 0
        dictData['ERR_CODE'] = 2999
        dictData['LAC_CELL'] = self.convertLacCell(dictData['LAC_ID'],dictData['CELL_ID'])
        
        if 'SESSION_ID' in dictData.keys():
            pass
        else:
            dictData['SESSION_ID'] = self.genSessionID()
        
    ###-------------API-------------------
        
    def needUpdate(self,dictData):
        isEnd = 0
        isNeed = True
        if ('DURATION' not in dictData.keys() and int(dictData['PROCESS_TYPE']) > 1 ) or \
           ('ADDUP_RES' in dictData.keys() and int(dictData['ADDUP_RES']) >= int(dictData['DURATION'])):
            dictData['PROCESS_TYPE'] = 3
            isEnd = 1
        elif 'STIME' in dictData.keys():
            if (time.time() - dictData['STIME']) * self.timerate >= int(dictData['ADDUP_RES']):
                print 'I am needupdate'
                if dictData['PROCESS_TYPE'] == 3:
                    isEnd = 1
                else:
                    pass
            else:
                isNeed = False
        if 'ERR_CODE' in dictData.keys() and int(dictData['ERR_CODE']) > 2999:
            isEnd = 2

        return (isNeed,isEnd)
    
    def updateData(self,command,byteData,dictData):
        dictData['CC_REQUEST_NUMBER'] += 1
        if dictData['PROCESS_TYPE'] == 1:  #init,416
            dictData['STIME'] = time.time()

        if 'DURATION' not in dictData.keys() or \
           int(dictData['ADDUP_RES']) >= int(dictData['DURATION']):
            dictData['PROCESS_TYPE'] = 3  #termination
        else:
            dictData['PROCESS_TYPE'] = 2  #update

#        answerStr = prtCdr(self.pkgFormt[(command,'A')],len(byteData),byteData)        #报文转成xdr
#        answerDict = str2Dict('fvpair',answerStr)
        answerDict = PrtCdrDict(self.pkgFormt[(command,'A')],len(byteData),byteData)
        dictData['ERR_CODE'] = int(answerDict['ERR_CODE'])
        if int(answerDict['ERR_CODE']) >= 2000 and int(answerDict['ERR_CODE']) < 3000 and \
           'IN_BUDGET_RES' in answerDict.keys():
            dictData['ADDUP_RES'] = int(dictData['ADDUP_RES']) + int(answerDict['IN_BUDGET_RES'])
        
    def getDccStream(self,command,dictData):
        """xdr dict转成dcc报文
        """
        flag = 'R'
        if command == 272:
            if dictData['PROCESS_TYPE'] == 1:
                callType = dictData['CALL_TYPE']
                if int(callType) == 0:  #moc
                   flag = 'IO'
                elif int(callType) == 1:  #mtc
                   flag = 'IT'
                elif int(callType) == 3:  #mfc
                   flag = 'IF'
                else:
                   flag = 'IO'
                flag = 'R' + flag
                self.analyse(dictData)
            elif dictData['PROCESS_TYPE'] == 2:
                flag = 'RU'
            else:
                flag = 'RT'
        
        if command == 258:
            flag = 'A'

#        strData = self.dict2FV(dictData)
#        retbuffer = bytearray(2048)
        ret = GetDccMsg(self.pkgFormt[(command,flag)],dictData,command)
        # return ret.encode()
        return ret

    def prtDccStr(self,command,byteData):
       """报文解码转成xdr dict
       """
       flag = 'A'
       if command == 258:
           flag = 'R'

       rltstr = ''
       if self.logflag == 0:
           rltstr = prtDcc(len(byteData),byteData)
       elif self.logflag == 1:
           rltstr = dumpDcc(self.pkgFormt[(command,flag)],len(byteData),byteData)
       else:
           rltstr = PrtCdrDict(self.pkgFormt[(command,flag)],len(byteData),byteData)

       return rltstr
        


